## Week 3 Goals
* [ ] Create an Issue and Milestone
* [ ] Explore epics, Value Stream Analytics, and Analytics at the project level
* [ ] Create a Values Stream and track a change through each stage

## Async Work
<details><summary> Click to expand </summary>

### The GitLab Flow - Create an Issue
![Screen_Shot_2023-01-10_at_4.34.51_PM](/uploads/e7f5238f3d4737008174c78df04272d8/Screen_Shot_2023-01-10_at_4.34.51_PM.png)

This is the recommended flow we provide to customers. We are primarily focusing on the first step, which is "Create an Issue." However, this week we will go deeper than just issue creation, and into the broader value stream process of GitLab and planning features that matter to our customers.

### [Issues](https://docs.gitlab.com/ee/user/project/issues/)
"Everything starts as an Issue at GitLab." An issue can be viewed as a single unit of work, where planning occurs through the issue Description, or text body, and comments on the issue from various participants. An issue exists at the Project level. There are some key components to understand in an issue:

- Assignees
   - An issue can have 0 or more assignees, who are responsible for the issue. 
- Epic
   - We will discuss epics in a moment, but they are group level collections of issues in various projects.
- Labels
   - Labels help you organize Issues. You can create a project-level or group-level label that projects inherit. Labels are customizable by name as well as color, i.e. you can create a label with a name such as "meeting agenda" with the color blue. When you search through your issues, you can search by label and see only issues associated with that label. Labels can also be scoped, i.e. meeting::agenda.
- Milestone and Iteration
   - Iterations are a way to track issues over a period of time, while milestones allow you to organize issues and merge requests into a cohesive group, with an optional start date and an optional due date.
- Weight
   - Weight allows you to assign a weight value to an issue. This is often used to estimate the amount of work in "story points" for an issue.
- Due Date
   - You can add a due date to an issue.
- Time Tracking
   - Time tracking aggregates the time spent on an issue that developers can log using the `/spend` quick action. Some companies require you to track time spent on a merge request or issue as the total time spent can aid in weighting future issues. The time tracking tracks manual time entries.
- Health status
   - Health status allows for a quick way to update and view the health of an issue. These statuses are updated manually and are not customizable.
- Confidentiality
   - Issues can be made confidential, which means the issue is only visible to project members with at least the Reporter role. Guests can create confidential issues, but only view the ones they have created. A customer may want to create a confidential issue from a vulnerability record as they may not want to expose information on the vulnerabilities resolution plan or related merge requests to Guests in their GitLab instance or namespace.
- Lock issue
   - When you lock an issue, only project members can see the issue.
- Notifications
   - You can disable notifications on an issue by using the Notifications toggle. This setting only applies to you, as you cannot enable/disable notifications for other users. Read more about notifications [here](https://docs.gitlab.com/ee/user/profile/notifications.html#who-receives-notifications-on-issues-merge-requests-and-epics). 
- Participants
   - Users are automatically added to the participants list if they interact with an issue. This may include commenting on an issue, reacting to it with an emoji, or other actions.


### [Epics](https://docs.gitlab.com/ee/user/group/epics/)
Epics help you manage a collection of issues and milestones by consolidating them by theme. Since the issues associated with an epic can span projects in a group, epics exist at the group level.

The possible relationships between epics and issues are:

- An epic is the parent of one or more issues.
- An epic is the parent of one or more child epics. For details see [Multi-level child epics](https://docs.gitlab.com/ee/user/group/epics/manage_epics.html#multi-level-child-epics).

The second relationship is exclusive to GitLab Ultimate.

If an epic has one or more child epics, it can be viewed as a Roadmap to visaulize the epics progress over time. Here is an example:

![epic](/uploads/7eba2d2eb92e865a4a31b6d5d583f3b4/epic.png)

In this example, you can see the two epics, as well as the percentage completion of the epics, and their start and end dates.

### [Issue Boards](https://docs.gitlab.com/ee/user/project/issue_board.html)

Now that you have issues to capture work, how do you organize it all? Issue boards pair issue tracking and project management, keeping everything together, so you can organize your workflow on a single platform. Boards allow for organizing issues visually into lists. A list is a column in the board that shows issues in that column. Each list has title, which can be a label, assignees, milestones, or plain text. The title controls which issues appear in the list, i.e. an issue with a `to-do` label would appear in a list titled `to-do` in the issue board. If the label changes from `to-do` to `doing`, it would appear under the list titled `doing`. 

You can create multiple issue boards in a project as well multiple in a group (on Premium and higher). 

![issueboard](/uploads/70d2308f5ac8392e633cde0bde79224c/issueboard.png)


### [DORA4 Metrics](https://docs.gitlab.com/ee/user/analytics/dora_metrics.html)
Once you establish a project management process, you want to be able to define what "good" looks like. The DevOps Research and Assessment (DORA) metrics are becoming the DevOps standard for measuring both **velocity** and **stability** of the development of applications. The DORA4 are 4 metrics as follows:

1. Deployment frequency (velocity)
   - Deployment frequency is the frequency of successful deployments to production (hourly, daily, weekly, monthly, or yearly) 
1. Lead time for changes (velocity)
   - DORA Lead time for changes measures the time to successfully deliver a commit into production. This metric reflects the efficiency of CI/CD pipelines. 
1. Change failure rate (stability)
   - Change failure rate measures the percentage of deployments that cause a failure in production. GitLab measures this as the number of incidents divided by the number of deployments to a production environment in the given time period.  
1. Mean time to recover (stability)
   - Time to restore service measures how long it takes an organization to recover from a failure in production. GitLab measures this as the average time required to close the incidents in the given time period.

It is important to ask your customers if they are currently tracking the DORA4 and using them to measure the success of their applications. If not, customers can begin tracking the DORA4 via various Project Analytics as well as Value Stream Analytics, and they can even build custom reports using the [DORA4 Key Metrics API](https://docs.gitlab.com/ee/api/dora/metrics.html). Additionally, our executive dashboards showcase DORA4 trends over time, which provides great insight to key executive personas.

### Analytics
We offer many analytics in GitLab. Some highlights include:




| Type | Analytics to Note | Customer Ask
| ------ | ------ | ------ |
| [CI/CD Analytics](https://docs.gitlab.com/ee/user/analytics/ci_cd_analytics.html) | Deployment frequency, lead time for changes, time to restore service, change failure rate | View and understand DORA4, CI/CD information
| [Code Review Analytics](https://docs.gitlab.com/ee/user/analytics/code_review_analytics.html) | Number of comments in an MR, MR duration | Improve code review process | 
| [Contribution Analytics](https://docs.gitlab.com/ee/user/group/contribution_analytics/) | Number of push events, merge requests, and closed issues | Understand development activity and developer productivity per team |
| [Issue Analytics](https://docs.gitlab.com/ee/user/analytics/issue_analytics.html) (Group or Project) | No. issues created per month | Understand work generated for a project or for projects in a group
| [Repository Analytics](https://docs.gitlab.com/ee/user/analytics/repository_analytics.html) | Programming languages used in a repository, commit statistics, code coverage history | See overview of a repository's contents and activity

### Value Stream Analytics
A value stream is the entire work process that delivers value to customers. We can track the time spent in each stage of the development process using Value Stream Analytics (VSAs). VSAs exist at both the project and group level. You can see how VSAs are measured at the project level [here](https://docs.gitlab.com/ee/user/analytics/value_stream_analytics.html#how-value-stream-analytics-measures-each-stage).

### Our Competitors
Some of our top Plan competitors are:
- JIRA/Atlassian Suite
- Trello
- ServiceNow
- AzureDevOps Boards

</details>

## Exercise 1: Configure Your Value Stream

<details><summary>Click to expand</summary>

Let's set Value Stream at the group level so we can see analytics in action.

- [ ] Navigate to your top level group, which is called `gl-demo-ultimate-{FIRSTINITIAL_LASTNAME}`
- [ ] In the left pane, navigate to Analytics -> Value Stream
- [ ] Select `Create custom value a stream`
- [ ] Enter a value stream name such as `My Ultimate Value Stream`
   - Keep the default settings and read over each stage to familiarize yourself with the start and end of each stage.
- [ ] Select `Create value stream`
   - Allow for data to be collected and pay attention to when the data was last updated. 

*Continue to Exercise 2*

</details>

## Exercise 2: Create an Issue and Track Through Value Stream Workflow
<details><summary>Click to expand</summary>

Now that we have our value stream configured, we are going to work with issues and track changes through the workflow.

Create Your First Issue
- [ ] Navigate to your Ultimate demo project. 
- [ ] Navigate to your Issues and select `New issue`
- [ ] Add a title such as `My First Issue`
- [ ] Optional: Add a description of your choice
- [ ] Add a label to your issue by selecting the `Labels` drop down and `Create project label`
   - [ ] Create a label called my-label with any color of your choosing.
   - [ ] Select the label so it is applied to your issue. 

Create Your First Milestone
- [ ] In the left pane, navigate to Issues -> Milestones
- [ ] Select `New milestone`
- [ ] Add a title such as `Sprint 1`
- [ ] Select today as the start date
- [ ] Select 2 weeks from now as the end date
- [ ] Optional: Add a description of your choice
- [ ] Navigate back to `My First Issue`
- [ ] Select `Edit` next to Milestone and select your `Sprint 1` milestone
- [ ] Note the issue ID at the top of your screen. It will say Group Name > Project Name > Issues > #ISSUE_ID. The issue ID is a number.

Mention `My First Issue` in a Commit and Create MR
- [ ] Select the dropdown on `Create Merge Request` and select `Create branch`
- [ ] There should be a branch created with an issue number and the name of the issue, such as `1-my-first-issue` if the issue is called My First Issue and the number is 1.
- [ ] Navigate to the Web IDE.
- [ ] Update index.html and add the remaining files in the [Week 3 snippet](https://gitlab.com/smorris-secure-app-demo1/ultimate-demo-template/-/snippets/2488508)
- [ ] Create a folder inside public called `assets` and upload favicon.ico file inside.
- [ ] Create a folder called `img` inside assets and upload the image files to the folder.

Your Changes Should Look Like This:
![Screen_Shot_2023-01-24_at_4.49.13_PM](/uploads/c3cfe69d8655af4e40cf5233a35bbba7/Screen_Shot_2023-01-24_at_4.49.13_PM.png)

- [ ] Commit your changes to your issue branch with a commit message containing your issue ID.
   - For example, if your issue ID is 1, your commit message should be `#1 update site contents`
- [ ] Push your changes to the branch
- [ ] Select `Create Merge Request` in the bottom right corner

Merge Your MR
- [ ] After your MR pipeline runs successfully, check the deployment of the review app to confirm your blog site looks as expected
- [ ] Assign @sam as a reviewer
- [ ] Select `Merge` on your MR

Evaluate Your Value Stream
- [ ] Wait 15 minutes or so for the Value Stream to refresh, then navigate back to your Group -> Analytics -> Value Stream
- [ ] Filter by your project `{YOUR_NAME Ultimate Demo}`
- [ ] Click through each stage to see your change navigate through the Value Stream


*Stop for now until the live discussion.*

### Plan Features only available in Ultimate
- Quality Management - Define test cases and build a backlog of work based on failed tests
- Status Page - Deploy a static page to communicate with application stakeholders
- Requirements Management - Gather, document, refine, track and approve business and system requirements
- Company wide portfolio management - Plan and track work at the portfolio and project level in order to manage resources and capacity 
- Issue and Epic Health Reporting - Report on and respond to the health of work objects
- Portfolio level Roadmaps - Establish product vision and strategy and gain progress insights and more
- Group and Project Insights - Charts to visualize performance data 

</details>

## Next Steps
Live Discussion:
- Take a look at the value stream
- Create an epic and associate the issue created this week

### Next Up: [Secure](#5)

~content
